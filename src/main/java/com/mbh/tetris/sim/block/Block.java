package com.mbh.tetris.sim.block;

public class Block {
    public BlockColor color;

    public Block(BlockColor color) {
        this.color = color;
    }

    public enum BlockColor {
        CYAN, BLUE, ORANGE, YELLOW, GREEN, PURPLE, RED
    }

    public static class Position {
        public int row;
        public int col;

        public Position(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }

    public static class BlockWithPosition {
        public final Block block;
        public final Position position;

        public BlockWithPosition(Block block, Position position) {
            this.block = block;
            this.position = position;

        }
    }
}

