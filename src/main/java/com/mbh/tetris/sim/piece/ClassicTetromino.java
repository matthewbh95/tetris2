package com.mbh.tetris.sim.piece;

import java.util.Optional;
import java.util.function.Function;

public class ClassicTetromino extends Tetromino {
    public ClassicTetromino(Type type, int row, int col, int orientation) {
        super(type, row, col, orientation);
    }

    @Override
    public Tetromino create(Type type, int row, int col, int orientation) {
        return new ClassicTetromino(type, row, col, orientation);
    }

    @Override
    protected Optional<Piece> getRotatedTo(int newOrientation, Function<Piece, Boolean> isPieceValidFn) {
        int[][][] kickTable = this.getType().kickOffsetTable;
        int rowOffset = kickTable[this.getOrientation()][0][0] - kickTable[newOrientation][0][0];
        int colOffset = kickTable[this.getOrientation()][0][1] - kickTable[newOrientation][0][1];
        Piece candidatePiece = create(this.getType(), this.getRow() + rowOffset, this.getCol() + colOffset, newOrientation);
        if (isPieceValidFn.apply(candidatePiece)) {
            return Optional.of(candidatePiece);
        }
        return Optional.empty();
    }

    public static class ClassicTetrominoFactory implements TetrominoFactory {
        @Override
        public Tetromino createTetromino(Type type) {
            return new ClassicTetromino(type, 0, 0, 0);
        }
    }
}
