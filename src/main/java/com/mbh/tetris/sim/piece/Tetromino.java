package com.mbh.tetris.sim.piece;

import com.mbh.tetris.sim.block.Block;

import java.util.Iterator;
import java.util.Optional;
import java.util.function.Function;

public class Tetromino implements Piece{

    private static final int[][] I_BLOCKS = {{0, -1}, {0, 0}, {0, 1}, {0, 2}};
    private static final int[][] J_BLOCKS = {{1, -1}, {0, -1}, {0, 0}, {0, 1}};
    private static final int[][] L_BLOCKS = {{1, 1}, {0, -1}, {0, 0}, {0, 1}};
    private static final int[][] O_BLOCKS = {{1, 0}, {1, 1}, {0, 0}, {0, 1}};
    private static final int[][] S_BLOCKS = {{1, 0}, {1, 1}, {0, -1}, {0, 0}};
    private static final int[][] T_BLOCKS = {{1, 0}, {0, -1}, {0, 0}, {0, 1}};
    private static final int[][] Z_BLOCKS = {{1, -1}, {1, 0}, {0, 0}, {0, 1}};

    /*
    Indices for offset tables are [STATE][ATTEMPT #][ROW (0) / COL (1)]
    Rotating from state A to state B calculate the offset sequence by taking
    (offset row, offset col) := (table[A][n][0] - table[B][n][0], table[A][n][1] - table[B][n][1])
    where each n is attempted in order 0 ... N
     */
    private static final int[][][] O_KICK_OFFSET_TABLE = {
            {{0, 0}}, // INITIAL
            {{-1, 0}}, // RIGHT
            {{-1, -1}}, // 2 ROTATIONS
            {{0, -1}}  // LEFT
    };
    private static final int[][][] I_KICK_OFFSET_TABLE = {
            {{0, 0}, {0, -1}, {0, 2}, {0, -1}, {0, 2}}, // INITIAL
            {{0, -1}, {0, 0}, {0, 0}, {1, 0}, {-2, 0}}, // RIGHT
            {{1, -1}, {1, 1}, {1, -2}, {0, 1}, {0, -2}}, // 2 ROTATIONS
            {{1, 0}, {1, 0}, {1, 0}, {-1, 0}, {2, 0}}  // LEFT
    };
    private static final int[][][] T_L_J_S_Z_KICK_OFFSET_TABLE = {
            {{0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}}, // INITIAL
            {{0, 0}, {0, 1}, {-1, 1}, {2, 0}, {2, 1}}, // RIGHT
            {{0, 0}, {0, 0}, {0, 0}, {0, 0}, {0, 0}}, // 2 ROTATIONS
            {{0, 0}, {0, -1}, {-1, -1}, {2, 0}, {2, -1}}  // LEFT
    };

    @Override
    public Iterator<Block.BlockWithPosition> iterator() {
        return new TetrominoBlockIterator(this);
    }

    public enum Type {
        I(I_BLOCKS, I_KICK_OFFSET_TABLE, Block.BlockColor.CYAN),
        J(J_BLOCKS, T_L_J_S_Z_KICK_OFFSET_TABLE, Block.BlockColor.BLUE),
        L(L_BLOCKS, T_L_J_S_Z_KICK_OFFSET_TABLE, Block.BlockColor.ORANGE),
        O(O_BLOCKS, O_KICK_OFFSET_TABLE, Block.BlockColor.YELLOW),
        S(S_BLOCKS, T_L_J_S_Z_KICK_OFFSET_TABLE, Block.BlockColor.GREEN),
        T(T_BLOCKS, T_L_J_S_Z_KICK_OFFSET_TABLE, Block.BlockColor.PURPLE),
        Z(Z_BLOCKS, T_L_J_S_Z_KICK_OFFSET_TABLE, Block.BlockColor.RED);

        final int[][] blockPositions;
        final int[][][] kickOffsetTable;
        final Block block;

        Type(int[][] blockPositions, int[][][] kickOffsetTable, Block.BlockColor color) {
            this.blockPositions = blockPositions;
            this.kickOffsetTable = kickOffsetTable;
            this.block = new Block(color);
        }
    }

    private final Type type;
    private final int row;
    private final int col;
    private final int orientation;

    public Tetromino(Type type, int row, int col, int orientation) {
        this.type = type;
        this.row = row;
        this.col = col;
        this.orientation = orientation;
    }

    public Tetromino create(Type type, int row, int col, int orientation) {
        return new Tetromino(type, row ,col, orientation);
    }

    public Type getType() {
        return type;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public int getOrientation() {
        return orientation;
    }

    @Override
    public Optional<Piece> getAt(int row, int col, int orientation, Function<Piece, Boolean> isPieceValidFn) {
        Tetromino newPiece = create(this.type, row, col, orientation);
        if (isPieceValidFn.apply(newPiece)) {
            return Optional.of(newPiece);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Piece> getTranslated(int dRow, int dCol, Function<Piece, Boolean> isPieceValidFn) {
        Tetromino translatedPiece = create(this.type, this.row + dRow, this.col + dCol, this.orientation);
        if (isPieceValidFn.apply(translatedPiece)) {
            return Optional.of(translatedPiece);
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Piece> getRotatedCW(Function<Piece, Boolean> isPieceValidFn) {
        return this.getRotatedTo((this.orientation + 1) % 4, isPieceValidFn);

    }

    @Override
    public Optional<Piece> getRotatedCCW(Function<Piece, Boolean> isPieceValidFn) {
        return this.getRotatedTo((this.orientation + 3) % 4, isPieceValidFn);
    }

    protected Optional<Piece> getRotatedTo(int newOrientation, Function<Piece, Boolean> isPieceValidFn) {
        int[][][] kickTable = this.type.kickOffsetTable;
        for (int i = 0; i < kickTable[this.orientation].length; i++) {
            int rowOffset = kickTable[this.orientation][i][0] - kickTable[newOrientation][i][0];
            int colOffset = kickTable[this.orientation][i][1] - kickTable[newOrientation][i][1];
            Piece candidatePiece = create(this.type, this.row + rowOffset, this.col + colOffset, newOrientation);
            if (isPieceValidFn.apply(candidatePiece)) {
                return Optional.of(candidatePiece);
            }
        }
        return Optional.empty();
    }

    private Block.BlockWithPosition getBlockByIndex(int index) {
        if (index < 0 || index >= 4) {
            throw new IndexOutOfBoundsException("Index out of bounds: " + index);
        }

        int localRow = this.type.blockPositions[index][0];
        int localCol = this.type.blockPositions[index][1];

        int rotatedRow = localRow;
        int rotatedCol = localCol;

        switch (this.orientation % 4) {
            case 1:
                rotatedRow = -localCol;
                rotatedCol = localRow;
                break;
            case 2:
                rotatedRow = -localRow;
                rotatedCol = -localCol;
                break;
            case 3:
                rotatedRow = localCol;
                rotatedCol = -localRow;
                break;
            default:
                break;
        }

        Block.Position globalPos = new Block.Position(rotatedRow + this.row, rotatedCol + this.col);

        return new Block.BlockWithPosition(type.block, globalPos);
    }

    public static class TetrominoBlockIterator implements Iterator<Block.BlockWithPosition> {

        private int nextIndex;
        private Tetromino tetromino;

        public TetrominoBlockIterator(Tetromino tetromino) {
            this.nextIndex = 0;
            this.tetromino = tetromino;
        }

        @Override
        public boolean hasNext() {
            return nextIndex >= 0 && nextIndex < 4;
        }

        @Override
        public Block.BlockWithPosition next() {
            return this.tetromino.getBlockByIndex(nextIndex++);
        }
    }

    public interface TetrominoFactory {
        Tetromino createTetromino(Type type);
    }

    public static class StandardTetrominoFactory implements TetrominoFactory {
        @Override
        public Tetromino createTetromino(Type type) {
            return new Tetromino(type, 0, 0, 0);
        }
    }
}
