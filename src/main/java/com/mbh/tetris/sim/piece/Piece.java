package com.mbh.tetris.sim.piece;

import com.mbh.tetris.sim.block.Block;

import java.util.Optional;
import java.util.function.Function;

public interface Piece extends Iterable<Block.BlockWithPosition> {
    Optional<Piece> getAt(int row, int col, int orientation, Function<Piece, Boolean> isValidFn);
    Optional<Piece> getTranslated(int dRow, int dCol, Function<Piece, Boolean> isPieceValidFn);
    Optional<Piece> getRotatedCW(Function<Piece, Boolean> isPieceValidFn);
    Optional<Piece> getRotatedCCW(Function<Piece, Boolean> isPieceValidFn);
}
