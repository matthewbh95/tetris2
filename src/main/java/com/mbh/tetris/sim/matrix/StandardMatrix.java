package com.mbh.tetris.sim.matrix;

import com.mbh.tetris.sim.block.Block;
import com.mbh.tetris.sim.piece.Piece;

import java.util.Optional;

public class StandardMatrix extends ArrayBasedMatrix {

    public static final int WIDTH = 10;
    public static final int HEIGHT = 40;

    public StandardMatrix() {
        super(WIDTH, HEIGHT);
    }
}
