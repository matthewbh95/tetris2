package com.mbh.tetris.sim.matrix;

import com.mbh.tetris.sim.block.Block;
import com.mbh.tetris.sim.piece.Piece;

import java.util.Optional;

public class ArrayBasedMatrix implements Matrix {

    private int width;
    private int height;
    private Block[][] blocks;

    public ArrayBasedMatrix(int width, int height) {
        this.blocks = new Block[height][width];
        this.width = width;
        this.height = height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public Optional<Block> getBlock(int r, int c) {
        return Optional.ofNullable(blocks[r][c]);
    }

    @Override
    public Optional<Block> clearBlock(int r, int c) {
        Optional<Block> prev = getBlock(r, c);
        blocks[r][c] = null;
        return prev;
    }

    @Override
    public Optional<Block> setBlock(int r, int c, Block b) {
        Optional<Block> prev = getBlock(r, c);
        blocks[r][c] = b;
        return prev;
    }

    @Override
    public Optional<Piece> getPieceMovedToSpawn(Piece piece) {
        return piece.getAt((height / 2) - 1, (width / 2) - 1, 0, this::isPieceValid);
    }

}
