package com.mbh.tetris.sim.matrix;

import com.mbh.tetris.sim.block.Block;
import com.mbh.tetris.sim.piece.Piece;

import java.util.*;
import java.util.stream.Collectors;

public interface Matrix {
    int getWidth();

    int getHeight();

    Optional<Block> getBlock(int r, int c);

    Optional<Block> clearBlock(int r, int c);

    Optional<Block> setBlock(int r, int c, Block b);

    Optional<Piece> getPieceMovedToSpawn(Piece piece);

    default Collection<Block.BlockWithPosition> getBlocks() {
        List<Block.BlockWithPosition> blocks = new ArrayList<>();
        for (int r = 0; r < this.getHeight(); r++) {
            for (int c = 0; c < this.getWidth(); c++) {
                Optional<Block> blockOptional = this.getBlock(r, c);
                if (blockOptional.isPresent()) {
                    blocks.add(new Block.BlockWithPosition(blockOptional.get(), new Block.Position(r, c)));
                }
            }
        }
        return blocks;
    }

    default boolean isPositionSolid(int r, int c) {
        return (!isPositionValid(r, c)) || getBlock(r, c).isPresent();
    }

    default boolean isPositionValid(int r, int c) {
        return r >= 0 && r < this.getHeight() && c >= 0 && c < this.getWidth();
    }

    default Optional<Block> setBlock(Block.BlockWithPosition b) {
        return this.setBlock(b.position.row, b.position.col, b.block);
    }

    default boolean isRowFull(int r) {
        for (int c = 0; c < this.getWidth(); c++) {
            if (!this.getBlock(r, c).isPresent()) {
                return false;
            }
        }
        return true;
    }

    default boolean isPieceValid(Piece piece) {
        for (Block.BlockWithPosition b : piece) {
            if (!isPositionValid(b.position.row, b.position.col) || getBlock(b.position.row, b.position.col).isPresent()) {
                return false;
            }
        }
        return true;
    }

    default Optional<PieceLockResult> setPiece(Piece piece) {
        if (!isPieceValid(piece)) {
            return Optional.empty();
        }
        Set<Integer> affectedRows = new HashSet<>();
        for (Block.BlockWithPosition b : piece) {
            setBlock(b);
            affectedRows.add(b.position.row);
        }

        List<Integer> sortedClearedRows = affectedRows.stream()
                .filter(this::isRowFull)
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());

        sortedClearedRows.forEach(this::clearRow);

        return Optional.of(new PieceLockResult(sortedClearedRows));
    }

    default void clearRow(int row) {
        // Copy every row above the cleared row down by one
        for (int r = row + 1; r < this.getHeight(); r++) {
            for (int c = 0; c < this.getWidth(); c++) {
                Optional<Block> block = this.getBlock(r, c);
                if (block.isPresent()) {
                    this.setBlock(r - 1, c, block.get());
                } else {
                    this.clearBlock(r - 1, c);
                }
            }
        }
        // Clear the top row of blocks
        for (int c = 0; c < this.getWidth(); c++) {
            this.clearBlock(this.getHeight() - 1, c);
        }
    }

    class PieceLockResult {
        public final Collection<Integer> rowsCleared;

        public PieceLockResult(Collection<Integer> rowsCleared) {
            this.rowsCleared = rowsCleared;
        }
    }
}
