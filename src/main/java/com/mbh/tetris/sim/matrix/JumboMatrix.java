package com.mbh.tetris.sim.matrix;

import com.mbh.tetris.sim.block.Block;
import com.mbh.tetris.sim.piece.Piece;

import java.lang.reflect.Array;
import java.util.Optional;

public class JumboMatrix extends ArrayBasedMatrix {

    public static final int WIDTH = 6;
    public static final int HEIGHT = 14;
    public JumboMatrix() {
        super(WIDTH, HEIGHT);
    }
}
