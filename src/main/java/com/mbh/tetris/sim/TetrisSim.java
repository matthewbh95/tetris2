package com.mbh.tetris.sim;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.mbh.tetris.sim.matrix.Matrix;
import com.mbh.tetris.sim.piece.Piece;

import java.util.Optional;

public class TetrisSim {
    private final Matrix matrix;
    private Piece currentPiece;

    @Inject
    public TetrisSim(Matrix matrix, @Assisted Piece firstPiece) {
        this.matrix = matrix;
        if (!this.spawnPiece(firstPiece)) {
            throw new RuntimeException("Failed to spawn first piece.");
        }
    }

    public Piece getCurrentPiece() {
        return this.currentPiece;
    }

    public Matrix getMatrix() {
        return this.matrix;
    }

    public boolean spawnPiece(Piece piece) {
        Optional<Piece> spawnedPiece = this.matrix.getPieceMovedToSpawn(piece);
        if (spawnedPiece.isPresent()) {
            this.currentPiece = spawnedPiece.get();
            return true;
        }
        return false;
    }

    public boolean movePieceUp() {
        return this.movePiece(1, 0);
    }

    public boolean movePieceDown() {
        return this.movePiece(-1, 0);
    }

    public boolean movePieceLeft() {
        return this.movePiece(0, -1);
    }

    public boolean movePieceRight() {
        return this.movePiece(0, 1);
    }

    private boolean movePiece(int dRow, int dCol) {
        Optional<Piece> translated = this.currentPiece.getTranslated(dRow, dCol, this.matrix::isPieceValid);
        if (translated.isPresent()) {
            this.currentPiece = translated.get();
            return true;
        } else {
            return false;
        }
    }

    public boolean rotatePieceCW() {
        Optional<Piece> rotated = this.currentPiece.getRotatedCW(this.matrix::isPieceValid);
        if (rotated.isPresent()) {
            this.currentPiece = rotated.get();
            return true;
        } else {
            return false;
        }
    }

    public boolean rotatePieceCCW() {
        Optional<Piece> rotated = this.currentPiece.getRotatedCCW(this.matrix::isPieceValid);
        if (rotated.isPresent()) {
            this.currentPiece = rotated.get();
            return true;
        } else {
            return false;
        }
    }

    public Optional<Matrix.PieceLockResult> lockPiece() {
        return this.matrix.setPiece(this.currentPiece);
    }

    public boolean isPieceOnGround() {
        Optional<Piece> test = this.currentPiece.getTranslated(-1, 0, this.matrix::isPieceValid);
        return !test.isPresent();
    }

    public Piece getGhost() {
        Piece ghost = this.currentPiece;
        Optional<Piece> nextGhostCandidate = ghost.getTranslated(-1, 0, this.matrix::isPieceValid);
        while (nextGhostCandidate.isPresent()) {
            ghost = nextGhostCandidate.get();
            nextGhostCandidate = ghost.getTranslated(-1, 0, this.matrix::isPieceValid);
        }
        return ghost;
    }
}
