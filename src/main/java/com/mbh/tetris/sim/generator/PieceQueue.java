package com.mbh.tetris.sim.generator;

import com.google.inject.Inject;
import com.mbh.tetris.sim.piece.Piece;

import java.util.ArrayList;
import java.util.List;

public class PieceQueue implements PieceGenerator {
    private PieceGenerator generator;

    private List<Piece> queue = new ArrayList<>();

    @Inject
    public PieceQueue(PieceGenerator generator) {
        this.generator = generator;
    }

    @Override
    public Piece next() {
        if (queue.size() < 1) {
            queue.add(generator.next());
        }
        return queue.remove(0);
    }

    public List<Piece> peek(int n) {
        while (queue.size() < n) {
            queue.add(generator.next());
        }
        return queue.subList(0, n);
    }
}
