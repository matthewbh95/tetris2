package com.mbh.tetris.sim.generator;

import com.google.inject.Inject;
import com.mbh.tetris.sim.piece.Piece;
import com.mbh.tetris.sim.piece.Tetromino;

import java.util.Random;

public class RandomTetrominoGenerator implements PieceGenerator {

    private Tetromino.TetrominoFactory factory;
    private Random rng;

    @Inject
    public RandomTetrominoGenerator(Tetromino.TetrominoFactory factory) {
        this.factory = factory;
        this.rng = new Random();
    }

    @Override
    public Piece next() {
        Tetromino.Type[] allTypes = Tetromino.Type.values();
        Tetromino.Type nextType = allTypes[rng.nextInt(allTypes.length)];
        return factory.createTetromino(nextType);
    }
}