package com.mbh.tetris.sim.generator;

import com.google.inject.Inject;
import com.mbh.tetris.sim.piece.Piece;
import com.mbh.tetris.sim.piece.Tetromino;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class BagTetrominoGenerator implements PieceGenerator {

    private Tetromino.TetrominoFactory factory;
    private int currentIndex = 0;
    private List<Tetromino.Type> bag = Arrays.asList(Tetromino.Type.values());

    @Inject
    public BagTetrominoGenerator(Tetromino.TetrominoFactory factory) {
        this.factory = factory;
        Collections.shuffle(bag);
    }

    @Override
    public Piece next() {
        if (currentIndex >= bag.size()) {
            Collections.shuffle(bag);
            currentIndex = 0;
        }
        return factory.createTetromino(bag.get(currentIndex++));
    }
}
