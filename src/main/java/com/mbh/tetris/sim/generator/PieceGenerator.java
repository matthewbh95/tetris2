package com.mbh.tetris.sim.generator;

import com.mbh.tetris.sim.piece.Piece;

public interface PieceGenerator {
    Piece next();
}
