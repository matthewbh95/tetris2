package com.mbh.tetris.sim;

import com.mbh.tetris.sim.piece.Piece;

public interface TetrisSimFactory {
    TetrisSim createTetrisSim(Piece firstPiece);
}
