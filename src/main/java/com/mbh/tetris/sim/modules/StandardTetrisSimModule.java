package com.mbh.tetris.sim.modules;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.mbh.tetris.sim.TetrisSim;
import com.mbh.tetris.sim.TetrisSimFactory;
import com.mbh.tetris.sim.generator.BagTetrominoGenerator;
import com.mbh.tetris.sim.generator.PieceGenerator;
import com.mbh.tetris.sim.matrix.Matrix;
import com.mbh.tetris.sim.matrix.StandardMatrix;
import com.mbh.tetris.sim.piece.Tetromino;

public class StandardTetrisSimModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();
        bind(Tetromino.TetrominoFactory.class).to(Tetromino.StandardTetrominoFactory.class);
        bind(Matrix.class).to(StandardMatrix.class);
        bind(PieceGenerator.class).to(BagTetrominoGenerator.class);
        install(new FactoryModuleBuilder().implement(TetrisSim.class, TetrisSim.class).build(TetrisSimFactory.class));
    }
}