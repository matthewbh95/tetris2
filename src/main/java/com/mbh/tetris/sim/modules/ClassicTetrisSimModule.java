package com.mbh.tetris.sim.modules;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.mbh.tetris.sim.TetrisSim;
import com.mbh.tetris.sim.TetrisSimFactory;
import com.mbh.tetris.sim.generator.PieceGenerator;
import com.mbh.tetris.sim.generator.RandomTetrominoGenerator;
import com.mbh.tetris.sim.matrix.Matrix;
import com.mbh.tetris.sim.matrix.StandardMatrix;
import com.mbh.tetris.sim.piece.ClassicTetromino;
import com.mbh.tetris.sim.piece.Tetromino;

public class ClassicTetrisSimModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();
        bind(Tetromino.TetrominoFactory.class).to(ClassicTetromino.ClassicTetrominoFactory.class);
        bind(Matrix.class).to(StandardMatrix.class);
        bind(PieceGenerator.class).to(RandomTetrominoGenerator.class);
        install(new FactoryModuleBuilder().implement(TetrisSim.class, TetrisSim.class).build(TetrisSimFactory.class));
    }
}