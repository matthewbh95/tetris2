package com.mbh.tetris;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.mbh.tetris.fe.GameView;
import com.mbh.tetris.game.modules.TetrisGameModule;
import com.mbh.tetris.input.modules.VirtualGamepadModule;
import com.mbh.tetris.logger.EventLogger;
import com.mbh.tetris.fe.swing.SwingFrontendModule;
import com.mbh.tetris.sim.modules.StandardTetrisSimModule;

public class Main {
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(
                new SwingFrontendModule(),
                new TetrisGameModule(),
                new VirtualGamepadModule(),
                new StandardTetrisSimModule(),
                new EventBusModule());
        injector.getInstance(EventLogger.class);
        injector.getInstance(GameView.class).run();
    }
}
