package com.mbh.tetris.logger;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.mbh.tetris.game.event.PieceLockEvent;

public class EventLogger {
    @Inject
    public EventLogger(EventBus eventBus) {
        eventBus.register(this);
    }

    @Subscribe
    public void logPieceLock(PieceLockEvent event) {
        if(event.lastPieceChange.isTSpin) {
            System.out.print("T-Spin ");
        }

        switch (event.pieceLockResult.rowsCleared.size()) {
            case 0:
                if(event.lastPieceChange.isTSpin) {
                    System.out.println("");
                }
                break;
            case 1:
                System.out.println("Single!");
                break;
            case 2:
                System.out.println("Double!");
                break;
            case 3:
                System.out.println("Triple!");
                break;
            case 4:
                System.out.println("TETRIS!!!!");
                break;
            default:
                System.out.println("TETRIS PLUS!!!!!");
        }
    }
}
