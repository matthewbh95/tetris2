package com.mbh.tetris;

public class Event {
    public final long timestamp;

    public Event() {
        timestamp = System.nanoTime();
    }
}
