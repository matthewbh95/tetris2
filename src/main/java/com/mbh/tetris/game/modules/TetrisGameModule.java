package com.mbh.tetris.game.modules;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.mbh.tetris.game.GameInstance;
import com.mbh.tetris.game.GameInstanceFactory;
import com.mbh.tetris.game.TetrisGame;

public class TetrisGameModule extends AbstractModule {
    @Override
    public void configure() {
        bind(GameInstance.class).to(TetrisGame.class);
        install(new FactoryModuleBuilder().implement(GameInstance.class, TetrisGame.class).build(GameInstanceFactory.class));

    }

}
