package com.mbh.tetris.game.event;

import com.mbh.tetris.sim.matrix.Matrix;

public class PieceLockEvent {
    public final Matrix.PieceLockResult pieceLockResult;
    public final PieceChangeEvent lastPieceChange;

    public PieceLockEvent(Matrix.PieceLockResult pieceLockResult, PieceChangeEvent lastPieceChange) {
        this.pieceLockResult = pieceLockResult;
        this.lastPieceChange = lastPieceChange;
    }
}
