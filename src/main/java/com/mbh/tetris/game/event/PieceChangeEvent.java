package com.mbh.tetris.game.event;

import com.mbh.tetris.sim.piece.Piece;

public class PieceChangeEvent {
    public final PieceChangeType type;
    public final boolean isTSpin;
    public final Piece prevPiece;
    public final Piece newPiece;

    public PieceChangeEvent(PieceChangeType type, Piece prevPiece, Piece newPiece, boolean isTSpin) {
        this.type = type;
        this.isTSpin = isTSpin;
        this.prevPiece = prevPiece;
        this.newPiece = newPiece;
    }

    @Override
    public String toString() {
        return String.format("%s%s", this.type, this.isTSpin ? " T-Spin" : "");
    }

    public enum PieceChangeType {
        MOVE_L, MOVE_R, ROTATE_CW, ROTATE_CCW, SOFT_DROP, HARD_DROP, GRAVITY_DROP, SPAWN_NEXT, HOLD
    }

}
