package com.mbh.tetris.game.event;

import com.mbh.tetris.Event;

public class GameEvent extends Event {
    public final Type type;

    public GameEvent(Type type) {
        this.type = type;
    }

    public enum Type {
        GAME_OVER
    }
}
