package com.mbh.tetris.game;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.mbh.tetris.game.event.GameEvent;
import com.mbh.tetris.game.event.PieceChangeEvent;
import com.mbh.tetris.game.event.PieceLockEvent;
import com.mbh.tetris.input.VirtualGamepad;
import com.mbh.tetris.input.event.TetrisActionEvent;
import com.mbh.tetris.sim.TetrisSim;
import com.mbh.tetris.sim.TetrisSimFactory;
import com.mbh.tetris.sim.block.Block;
import com.mbh.tetris.sim.generator.PieceQueue;
import com.mbh.tetris.sim.matrix.Matrix;
import com.mbh.tetris.sim.piece.Piece;
import com.mbh.tetris.sim.piece.Tetromino;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class TetrisGame implements GameInstance {
    private static final float gravityRate = 1.0f;
    private static final float lockCountdownLength = 1.0f;

    private TetrisSim sim;
    private PieceQueue pieceQueue;
    private Optional<Piece> holdPiece = Optional.empty();
    private boolean canHold = true;
    private float gravityCounter = 0.0f;
    private float lockCountdown = lockCountdownLength;
    private boolean isValidState = true;
    private PieceChangeEvent lastPieceChange;

    private EventBus eventBus;

    @Inject
    public TetrisGame(PieceQueue pieceQueue, TetrisSimFactory simFactory, EventBus eventBus) {
        this.pieceQueue = pieceQueue;
        this.sim = simFactory.createTetrisSim(pieceQueue.next());
        this.lastPieceChange = new PieceChangeEvent(PieceChangeEvent.PieceChangeType.SPAWN_NEXT, sim.getCurrentPiece(), sim.getCurrentPiece(), false);
        eventBus.register(this);
        this.eventBus = eventBus;
    }

    @Override
    public Collection<Block.BlockWithPosition> getMatrixBlocks() {
        return this.sim.getMatrix().getBlocks();
    }

    @Override
    public Piece getCurrentPiece() {
        return this.sim.getCurrentPiece();
    }

    @Override
    public Piece getGhostPiece() {
        return this.sim.getGhost();
    }

    @Override
    public Optional<Piece> getHoldPiece() {
        return this.holdPiece;
    }

    @Override
    public List<Piece> getNextPieces(int n) {
        return this.pieceQueue.peek(n);
    }

    @Override
    public boolean isValidState() {
        return this.isValidState;
    }

    @Override
    public int getMatrixWidth() {
        return this.sim.getMatrix().getWidth();
    }

    @Override
    public int getMatrixHeight() {
        return this.sim.getMatrix().getHeight();
    }

    @Override
    public void update(float dt) {
        if (!this.isValidState()) {
            return;
        }

        this.gravityCounter += dt * gravityRate;
        while (this.gravityCounter >= 1.0) {
            Piece prevPiece = this.sim.getCurrentPiece();
            if (this.movePieceDown()) {
                Piece newPiece = this.sim.getCurrentPiece();
                this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.GRAVITY_DROP, prevPiece, newPiece, false));
            }

            this.gravityCounter -= 1.0;
        }

        if (this.sim.isPieceOnGround()) {
            this.lockCountdown -= dt;
            if (this.lockCountdown <= 0) {
                this.lockPiece();
            }
        }
    }

    private void postPieceChange(PieceChangeEvent event) {
        this.lastPieceChange = event;
        this.eventBus.post(event);
    }

    @Subscribe
    public void consume(TetrisActionEvent tetrisActionEvent) {
        Piece prevPiece;
        if (!isValidState) {
            return;
        }
        switch (tetrisActionEvent.action) {
            case LEFT:
                this.movePieceLeft();
                break;
            case RIGHT:
                this.movePieceRight();
                break;
            case SOFT_DROP:
                prevPiece = this.sim.getCurrentPiece();
                if (this.movePieceDown()) {
                    this.gravityCounter = 0f;
                    Piece newPiece = this.sim.getCurrentPiece();
                    this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.SOFT_DROP, prevPiece, newPiece, false));
                }
                break;
            case ROTATE_CW:
                this.rotatePieceCW();
                break;
            case ROTATE_CCW:
                this.rotatePieceCCW();
                break;
            case HOLD:
                this.hold();
                break;
            case HARD_DROP:
                prevPiece = this.sim.getCurrentPiece();
                while (!this.sim.isPieceOnGround()) {
                    this.movePieceDown();
                }
                Piece newPiece = this.sim.getCurrentPiece();
                this.lockPiece();
                this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.HARD_DROP, prevPiece, newPiece, false));
                break;
        }
    }

    private void lockPiece() {
        Optional<Matrix.PieceLockResult> setPieceResult = this.sim.lockPiece();
        if (setPieceResult.isPresent()) {
            this.eventBus.post(new PieceLockEvent(setPieceResult.get(), this.lastPieceChange));
            this.spawnNextPiece();
        }
        this.canHold = true;
        this.lockCountdown = lockCountdownLength;

    }

    private boolean couldBeTSpin() {
        if (!(this.sim.getCurrentPiece() instanceof Tetromino)) {
            return false;
        }

        Tetromino currentTetromino = (Tetromino) this.sim.getCurrentPiece();

        if (currentTetromino.getType() != Tetromino.Type.T) {
            return false;
        }

        int filledCount = 0;
        for (int dRow : new int[]{-1, 1}) {
            for (int dCol : new int[]{-1, 1}) {
                int r = currentTetromino.getRow() + dRow;
                int c = currentTetromino.getCol() + dCol;
                if (this.sim.getMatrix().isPositionSolid(r, c)) {
                    ++filledCount;
                }
            }
        }
        return filledCount >= 3;
    }

    private boolean movePieceLeft() {
        Piece prevPiece = this.sim.getCurrentPiece();
        if (this.sim.movePieceLeft()) {
            Piece newPiece = this.sim.getCurrentPiece();

            this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.MOVE_L, prevPiece, newPiece, false));
            return true;
        }
        return false;
    }

    private boolean movePieceRight() {
        Piece prevPiece = this.sim.getCurrentPiece();
        if (this.sim.movePieceRight()) {
            Piece newPiece = this.sim.getCurrentPiece();
            this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.MOVE_R, prevPiece, newPiece, false));
            return true;
        }
        return false;
    }

    private boolean movePieceDown() {
        if (this.sim.movePieceDown()) {
            return true;
        }
        return false;
    }

    private boolean rotatePieceCW() {
        Piece prevPiece = this.sim.getCurrentPiece();
        if (this.sim.rotatePieceCW()) {
            boolean isTSpin = couldBeTSpin();

            Piece newPiece = this.sim.getCurrentPiece();
            this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.ROTATE_CW, prevPiece, newPiece, isTSpin));
            return true;
        }
        return false;
    }

    private boolean rotatePieceCCW() {
        Piece prevPiece = this.sim.getCurrentPiece();
        if (this.sim.rotatePieceCCW()) {
            boolean isTSpin = couldBeTSpin();

            Piece newPiece = this.sim.getCurrentPiece();
            this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.ROTATE_CCW, prevPiece, newPiece, isTSpin));
            return true;
        }
        return false;
    }

    private boolean spawnNextPiece() {
        Piece prevPiece = this.sim.getCurrentPiece();
        if (!this.sim.spawnPiece(pieceQueue.next())) {
            this.gameOver();
            return false;
        }
        Piece newPiece = this.sim.getCurrentPiece();
        this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.SPAWN_NEXT, prevPiece, newPiece, false));
        return true;
    }

    private boolean hold() {
        if (!canHold) {
            return false;
        }
        this.canHold = false;
        Piece curPiece = this.sim.getCurrentPiece();
        if (this.holdPiece.isPresent()) {
            if (!this.sim.spawnPiece(this.holdPiece.get())) {
                this.gameOver();
                return false;
            } else {
                this.holdPiece = curPiece.getAt(0, 0, 0, x -> true);
                Piece newPiece = this.sim.getCurrentPiece();
                this.postPieceChange(new PieceChangeEvent(PieceChangeEvent.PieceChangeType.HOLD, curPiece, newPiece, false));
                return true;
            }
        } else {
            this.holdPiece = curPiece.getAt(0, 0, 0, x -> true);
            return this.spawnNextPiece();
        }
    }

    private void gameOver() {
        this.isValidState = false;
        this.eventBus.post(new GameEvent(GameEvent.Type.GAME_OVER));
    }
}
