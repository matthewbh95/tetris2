package com.mbh.tetris.game;

import com.mbh.tetris.sim.block.Block;
import com.mbh.tetris.sim.piece.Piece;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface GameInstance {
    void update(float dt);
    Collection<Block.BlockWithPosition> getMatrixBlocks();
    Piece getCurrentPiece();
    Piece getGhostPiece();
    Optional<Piece> getHoldPiece();
    List<Piece> getNextPieces(int n);
    boolean isValidState();
    int getMatrixWidth();
    int getMatrixHeight();
}
