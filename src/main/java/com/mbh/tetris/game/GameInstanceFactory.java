package com.mbh.tetris.game;

public interface GameInstanceFactory {
    GameInstance createGameInstance();
}
