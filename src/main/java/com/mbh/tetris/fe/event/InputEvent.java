package com.mbh.tetris.fe.event;

public class InputEvent {
    public enum InputEventType {
        KEY_UP, KEY_DOWN
    }

    public final InputEventType type;
    public final int keyId;

    public InputEvent(InputEventType type, int keyId) {
        this.type = type;
        this.keyId = keyId;
    }

    @Override
    public String toString() {
        return String.format("%s : %d", type.toString(), keyId);
    }
}
