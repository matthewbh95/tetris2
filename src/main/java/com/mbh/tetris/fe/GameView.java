package com.mbh.tetris.fe;

import com.mbh.tetris.game.GameInstance;

public interface GameView {
    void run();
}
