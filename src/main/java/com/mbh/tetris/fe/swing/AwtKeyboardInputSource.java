package com.mbh.tetris.fe.swing;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.mbh.tetris.fe.event.InputEvent;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class AwtKeyboardInputSource implements KeyListener {

    private EventBus eventBus;

    @Inject
    public AwtKeyboardInputSource(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        eventBus.post(new InputEvent(InputEvent.InputEventType.KEY_DOWN, e.getKeyCode()));
    }

    @Override
    public void keyReleased(KeyEvent e) {
        eventBus.post(new InputEvent(InputEvent.InputEventType.KEY_UP, e.getKeyCode()));
    }
}
