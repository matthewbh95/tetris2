package com.mbh.tetris.fe.swing;

import com.google.inject.AbstractModule;
import com.mbh.tetris.fe.GameView;
import com.mbh.tetris.fe.swing.SwingTetrisGame;

public class SwingFrontendModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();
        bind(GameView.class).to(SwingTetrisGame.class);
    }
}
