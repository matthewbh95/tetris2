package com.mbh.tetris.fe.swing;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.mbh.tetris.fe.GameView;
import com.mbh.tetris.game.GameInstance;
import com.mbh.tetris.game.GameInstanceFactory;
import com.mbh.tetris.game.event.GameEvent;
import com.mbh.tetris.game.event.PieceLockEvent;
import com.mbh.tetris.input.VirtualGamepad;
import com.mbh.tetris.sim.block.Block;
import com.mbh.tetris.sim.piece.Piece;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferStrategy;
import java.util.List;

public class SwingTetrisGame implements GameView {
    private static final String TITLE = "Tetris";
    private static final int DEFAULT_WIDTH = 640;
    private static final int DEFAULT_HEIGHT = 640;
    private static final int X_OFFSET = 128;
    private static final int Y_OFFSET = 0;

    private static final long FRAME_LENGTH = (long) (1e9 / 60);

    private Canvas canvas;

    private GameInstanceFactory gameFactory;

    private AwtKeyboardInputSource keyboardInputSource;

    private VirtualGamepad virtualGamepad;

    private GameInstance game;

    @Inject
    public SwingTetrisGame(GameInstanceFactory gameFactory, AwtKeyboardInputSource keyboardInputSource, VirtualGamepad  virtualGamepad, EventBus eventBus) {
        this.gameFactory = gameFactory;
        this.keyboardInputSource = keyboardInputSource;
        this.virtualGamepad = virtualGamepad;
        eventBus.register(this);
    }

    private static void drawBlockAbsolute(int x, int y, int blockWidth, int blockHeight, Block
            block, Graphics2D g, boolean fill) {
        switch (block.color) {
            case CYAN:
                g.setColor(Color.CYAN);
                break;
            case BLUE:
                g.setColor(Color.BLUE);
                break;
            case ORANGE:
                g.setColor(Color.ORANGE);
                break;
            case YELLOW:
                g.setColor(Color.YELLOW);
                break;
            case GREEN:
                g.setColor(Color.GREEN);
                break;
            case PURPLE:
                g.setColor(Color.MAGENTA);
                break;
            case RED:
                g.setColor(Color.RED);
                break;
        }
        if (fill) {
            g.fill3DRect(x, y, blockWidth, blockHeight, true);
        } else {
            Color base = g.getColor();
            g.setColor(new Color(base.getRed(), base.getGreen(), base.getBlue(), 100));
            g.fillRect(x, y, blockWidth, blockHeight);
//            g.setColor(base);
//            g.drawRect(x, y, blockWidth, blockHeight);
        }
    }

    private void drawBlockInGrid(int xOffset, int yOffset, int blockWidth,
                                        int blockHeight, Block.BlockWithPosition block, Graphics2D g, boolean fill) {
        drawBlockAbsolute(xOffset + blockWidth * block.position.col, yOffset + blockHeight * ((this.game.getMatrixHeight()/2) - 1 - block.position.row), blockWidth, blockHeight, block.block, g, fill);
    }

    private void drawBlockLocally(int xOffset, int yOffset, int blockWidth,
                                         int blockHeight, Block.BlockWithPosition block, Graphics2D g, boolean fill) {
        drawBlockAbsolute(xOffset + blockWidth * block.position.col, yOffset - blockHeight * block.position.row, blockWidth, blockHeight, block.block, g, fill);
    }

    @Override
    public void run() {

        JFrame frame = new JFrame(TITLE);

        /* Make sure the program will exit when the JFrame is closed */
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        /* Resize the JFrame and its content pane */
        frame.getContentPane().setPreferredSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
        frame.pack();

        /* Center the JFrame on the screen */
        frame.setLocationRelativeTo(null);

        /* Create the canvas and add it to the JFrame */
        canvas = new Canvas();
        frame.getContentPane().add(canvas);
        canvas.setSize(frame.getContentPane().getSize());

        /* Make sure the user can see the JFrame */
        frame.setVisible(true);

        canvas.addKeyListener(keyboardInputSource);

        canvas.createBufferStrategy(2);
        BufferStrategy bufferStrategy = canvas.getBufferStrategy();

        game = gameFactory.createGameInstance();

        while (canvas.isDisplayable()) {
            long frameEnd = System.nanoTime() + FRAME_LENGTH;

            virtualGamepad.update(1f/60f);
            game.update(1f / 60f);
            render(1.0f / 60.0f);

            while (System.nanoTime() < frameEnd) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Subscribe
    public void handleGameEvent(GameEvent event) {
        if (event.type == GameEvent.Type.GAME_OVER) {
            System.out.println("GAME OVER");
        }
    }

    @Subscribe
    public void handlePieceLock(PieceLockEvent event) {

    }

    private void render(float dt) {

        BufferStrategy bufferStrategy = canvas.getBufferStrategy();

        Graphics2D g = (Graphics2D) bufferStrategy.getDrawGraphics();

        g.setColor(Color.BLACK);

        g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        for (int r = 0; r < game.getMatrixHeight()/2; r++) {
            for (int c = 0; c < game.getMatrixWidth(); c++) {
//                    g.setColor((r+c)%2 == 0?Color.DARK_GRAY:Color.GRAY);
                g.setColor(Color.DARK_GRAY);
                g.drawRect(X_OFFSET + 32 * c, Y_OFFSET + 32 * (20 - 1 - r), 32, 32);
            }
        }
        g.setColor(Color.YELLOW);
        g.drawRect(X_OFFSET, Y_OFFSET, 320, 640);
        for (Block.BlockWithPosition block : game.getMatrixBlocks()) {
            drawBlockInGrid(X_OFFSET, Y_OFFSET, 32, 32, block, g, true);
        }

        for (Block.BlockWithPosition block : game.getCurrentPiece()) {
            drawBlockInGrid(X_OFFSET, Y_OFFSET, 32, 32, block, g, true);
        }

        for (Block.BlockWithPosition block : game.getGhostPiece()) {
            drawBlockInGrid(X_OFFSET, Y_OFFSET, 32, 32, block, g, false);
        }

        g.setColor(Color.BLUE);
        g.drawRect(16, 16, 96, 96);
        game.getHoldPiece().ifPresent(holdPiece -> {
            for (Block.BlockWithPosition block : holdPiece) {
                drawBlockLocally(48, 16 + 32, 20, 20, block, g, true);
            }
        });

        g.setColor(Color.BLUE);
        List<Piece> next5 = game.getNextPieces(5);
        for (int i = 0; i < 5; i++) {
            Piece queuedPiece = next5.get(i);
            for (Block.BlockWithPosition block : queuedPiece) {
                drawBlockLocally(32 + 464, 32 + (16 + 96 * i), 20, 20, block, g, true);
            }
            g.drawRect(464, 16 + 96 * i, 96, 94);

        }
        g.dispose();

        bufferStrategy.show();
    }
}