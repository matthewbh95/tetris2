package com.mbh.tetris.input;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Inject;
import com.mbh.tetris.fe.event.InputEvent;
import com.mbh.tetris.input.event.TetrisActionEvent;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import static com.mbh.tetris.input.event.TetrisActionEvent.TetrisAction;

public class TetrisVirtualGamepad implements VirtualGamepad {

    public static final float KEY_REPEAT_RATE = 30f;
    public static final float KEY_REPEAT_DELAY = 0.2f;
    private Map<Integer, TetrisAction> inputMap;
    private Map<TetrisAction, VirtualKey> virtualKeyMap;
    private Set<VirtualKey> virtualKeys;
    private LinkedBlockingQueue<InputEvent> inputEventBuffer = new LinkedBlockingQueue<>();
    private EventBus eventBus;

    @Inject
    public TetrisVirtualGamepad(@TetrisInputMap Map<Integer, TetrisAction> inputMap, EventBus eventBus) {
        this.eventBus = eventBus;
        this.inputMap = inputMap;
        Joystick2 lrJoystick = new Joystick2(TetrisAction.LEFT, TetrisAction.RIGHT, KEY_REPEAT_RATE, KEY_REPEAT_DELAY);
        this.virtualKeyMap = new HashMap<>();
        this.virtualKeyMap.put(TetrisAction.LEFT, lrJoystick);
        this.virtualKeyMap.put(TetrisAction.RIGHT, lrJoystick);
        this.virtualKeyMap.put(TetrisAction.ROTATE_CW, new SwitchKey(TetrisAction.ROTATE_CW));
        this.virtualKeyMap.put(TetrisAction.ROTATE_CCW, new SwitchKey(TetrisAction.ROTATE_CCW));
        this.virtualKeyMap.put(TetrisAction.SOFT_DROP, new RepeaterKey(TetrisAction.SOFT_DROP, KEY_REPEAT_RATE, 0f));
        this.virtualKeyMap.put(TetrisAction.HARD_DROP, new SwitchKey(TetrisAction.HARD_DROP));
        this.virtualKeyMap.put(TetrisAction.HOLD, new SwitchKey(TetrisAction.HOLD));

        this.virtualKeys = this.virtualKeyMap.values().stream().collect(Collectors.toSet());

        eventBus.register(this);
    }

    @Override
    public void update(float dt) {
        // Drain any input events received since last update and route them to the mapped virtual keys
        List<InputEvent> inputEvents = new ArrayList<>();
        this.inputEventBuffer.drainTo(inputEvents);
        for (InputEvent inputEvent : inputEvents) {
            Optional<TetrisAction> actionOptional = Optional.ofNullable(this.inputMap.get(inputEvent.keyId));
            if (actionOptional.isPresent()) {
                TetrisAction action = actionOptional.get();
                switch (inputEvent.type) {
                    case KEY_UP:
                        virtualKeyMap.get(action).upEvent(action);
                        break;
                    case KEY_DOWN:
                        virtualKeyMap.get(action).downEvent(action);
                        break;
                }
            }
        }

        // Update all virtual keys and buffer their action events together
        List<TetrisAction> actionBuffer = new ArrayList<>();
        for (VirtualKey virtualKey : this.virtualKeys) {
            virtualKey.update(dt);
            virtualKey.drainTo(actionBuffer);
        }

        // For every action event emitted by buttons notify each subscriber
        for (TetrisAction tetrisAction : actionBuffer) {
            eventBus.post(new TetrisActionEvent(tetrisAction));
        }
    }

    @Subscribe
    public void consume(InputEvent inputEvent) {
        inputEventBuffer.add(inputEvent);
    }

    public @interface TetrisInputMap {
    }

    interface VirtualKey {
        void downEvent(TetrisAction action);

        void upEvent(TetrisAction action);

        void drainTo(List<TetrisAction> actionEventBuffer);

        default void update(float dt) {
        }
    }

    private static class SwitchKey implements VirtualKey {
        private boolean pressed = false;
        private boolean justPressed = false;
        private TetrisAction boundAction;

        public SwitchKey(TetrisAction boundAction) {
            this.boundAction = boundAction;
        }

        @Override
        public void downEvent(TetrisAction action) {
            if (action != boundAction) return;
            if (!pressed) {
                pressed = true;
                justPressed = true;
            }
        }

        @Override
        public void upEvent(TetrisAction action) {
            if (action != boundAction) return;
            if (pressed) {
                pressed = false;
                justPressed = false;
            }
        }

        @Override
        public void drainTo(List<TetrisAction> actionEventBuffer) {
            if (justPressed) {
                actionEventBuffer.add(boundAction);
                justPressed = false;
            }
        }
    }

    private static class Joystick2 implements VirtualKey {

        private final float repeatDelay;
        private final float repeatRate;

        private boolean pressed1 = false;
        private boolean pressed2 = false;

        private float delayCountdown;
        private float repeatCounter = 0;

        private TetrisAction action1;
        private TetrisAction action2;

        public Joystick2(TetrisAction action1, TetrisAction action2, float repeatRate, float repeatDelay) {
            this.repeatDelay = repeatDelay;
            this.repeatRate = repeatRate;
            this.action1 = action1;
            this.action2 = action2;
        }

        @Override
        public void downEvent(TetrisAction action) {
            if (action == action1) {
                if (pressed1) {
                    return;
                }
                pressed1 = true;
                pressed2 = false;
                repeatCounter = 1.0f;
                delayCountdown = repeatDelay;

            } else if (action == action2) {
                if (pressed2) {
                    return;
                }
                pressed1 = false;
                pressed2 = true;
                repeatCounter = 1.0f;
                delayCountdown = repeatDelay;
            }
        }

        @Override
        public void upEvent(TetrisAction action) {
            if (action == action1 && pressed1) {
                pressed1 = false;
                repeatCounter = 0;
                delayCountdown = repeatDelay;
            } else if (action == action2 && pressed2) {
                pressed2 = false;
                repeatCounter = 0;
                delayCountdown = repeatDelay;
            }
        }

        @Override
        public void drainTo(List<TetrisAction> actionEventBuffer) {
            while (this.repeatCounter >= 1.0f) {
                this.repeatCounter -= 1.0f;
                if (this.pressed1) {
                    actionEventBuffer.add(this.action1);
                } else if (this.pressed2) {
                    actionEventBuffer.add(this.action2);
                }
            }
        }

        @Override
        public void update(float dt) {
            if (this.pressed1 || pressed2) {
//                GameSystem.out.printf("countdown: %f, repeat buffer: %f\n", delayCountdown, repeatCounter);
//                GameSystem.out.printf("dt: %f, dt*rr: %f\n", dt, dt * repeatRate);
                if (this.delayCountdown > 0) {
                    this.delayCountdown -= dt;
                } else {
                    this.repeatCounter += dt * repeatRate;
                }
            }
        }
    }

    private static class RepeaterKey implements VirtualKey {

        private final float repeatDelay;
        private final float repeatRate;
        private boolean pressed = false;
        private float delayCountdown;
        private float repeatCounter = 0;
        private TetrisAction boundAction;

        public RepeaterKey(TetrisAction boundAction, float repeatRate, float repeatDelay) {
            this.repeatRate = repeatRate;
            this.repeatDelay = repeatDelay;
            this.boundAction = boundAction;
            this.reset();
        }

        private void reset() {
            this.pressed = false;
            this.delayCountdown = repeatDelay;
            this.repeatCounter = 0;
        }


        @Override
        public void downEvent(TetrisAction action) {
            if (action != boundAction) return;
            if (this.pressed) {
                return;
            }
            this.pressed = true;
            this.repeatCounter = 1.0f;
        }

        @Override
        public void upEvent(TetrisAction action) {
            if (action != boundAction) return;
            this.reset();
        }

        @Override
        public void drainTo(List<TetrisAction> actionEventBuffer) {
            while (this.repeatCounter >= 1.0f) {
                this.repeatCounter -= 1.0f;
                actionEventBuffer.add(this.boundAction);
            }
        }

        @Override
        public void update(float dt) {
            if (this.pressed) {
//                GameSystem.out.printf("countdown: %f, repeat buffer: %f\n", delayCountdown, repeatCounter);
//                GameSystem.out.printf("dt: %f, dt*rr: %f\n", dt, dt * repeatRate);

                if (this.delayCountdown > 0) {
                    this.delayCountdown -= dt;
                } else {
                    this.repeatCounter += dt * repeatRate;
                }
            }
        }
    }
}
