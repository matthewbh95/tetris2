package com.mbh.tetris.input.event;

public class TetrisActionEvent {

    public enum TetrisAction {
        LEFT, RIGHT, ROTATE_CW, ROTATE_CCW, SOFT_DROP, HARD_DROP, HOLD
    }

    public final TetrisAction action;

    public TetrisActionEvent(TetrisAction action) {
        this.action = action;
    }
}
