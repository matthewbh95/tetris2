package com.mbh.tetris.input;

public interface VirtualGamepad {
    void update(float dt);
}
