package com.mbh.tetris.input.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.mbh.tetris.input.event.TetrisActionEvent;
import com.mbh.tetris.input.TetrisVirtualGamepad;

import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;

public class InputMappingModule extends AbstractModule {
    @Provides @TetrisVirtualGamepad.TetrisInputMap
    public Map<Integer, TetrisActionEvent.TetrisAction> getTetrisInputMapping() {
        Map<Integer, TetrisActionEvent.TetrisAction> defaultInputMap = new HashMap<>();
        defaultInputMap.put(KeyEvent.VK_LEFT, TetrisActionEvent.TetrisAction.LEFT);
        defaultInputMap.put(KeyEvent.VK_RIGHT, TetrisActionEvent.TetrisAction.RIGHT);
        defaultInputMap.put(KeyEvent.VK_DOWN, TetrisActionEvent.TetrisAction.SOFT_DROP);
        defaultInputMap.put(KeyEvent.VK_SPACE, TetrisActionEvent.TetrisAction.HARD_DROP);
        defaultInputMap.put(KeyEvent.VK_UP, TetrisActionEvent.TetrisAction.ROTATE_CW);
        defaultInputMap.put(KeyEvent.VK_X, TetrisActionEvent.TetrisAction.ROTATE_CW);
        defaultInputMap.put(KeyEvent.VK_Z, TetrisActionEvent.TetrisAction.ROTATE_CCW);
        defaultInputMap.put(KeyEvent.VK_SHIFT, TetrisActionEvent.TetrisAction.HOLD);
        return defaultInputMap;
    }
}
