package com.mbh.tetris.input.modules;

import com.google.inject.AbstractModule;
import com.mbh.tetris.input.TetrisVirtualGamepad;
import com.mbh.tetris.input.VirtualGamepad;

public class VirtualGamepadModule extends AbstractModule {
    @Override
    public void configure() {
        install(new InputMappingModule());
        bind(VirtualGamepad.class).to(TetrisVirtualGamepad.class);
    }
}
