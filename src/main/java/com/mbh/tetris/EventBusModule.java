package com.mbh.tetris;

import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;

public class EventBusModule extends AbstractModule {
    @Override
    protected void configure() {
        super.configure();
        this.bind(EventBus.class).asEagerSingleton();
    }
}
